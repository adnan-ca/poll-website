from django.urls import path
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='polls-home'),
    path('<int:pk>/', views.DetailsView.as_view(), name='polls-detail'),
    path('<int:pk>/result/',
         views.ResultView.as_view(), name='polls-result'),
    path('<int:question_id>/vote/', views.vote, name='polls-vote'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
