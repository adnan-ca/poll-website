# README #


### What is this repository for? ###

* A Simple polls app which displays all the questions (except for those with published date in future) list and allows users to vote for each question and see their results as well. It also contains a **Dockerfile** which can be used to create a docker container for the app by following the below mentioned steps.

### How do I get set up? ###

* Make sure you have Docker installed and running. To download and install docker [click here](https://docs.docker.com/engine/install/ "Download Docker") .
* To download the repository click on `Download Repository` as shown below :
![Download Repository](readme_images/img1.png) 
&nbsp;

* Unzip the zip file and move to the folder using the terminal/cmd.
* Run `ls` in the terminal to make sure current working directory contains **Dockerfile**
* Run `docker build -t mypollsapp .` in terminal to build the container image with name **mypollsapp**
* Run `docker run -it -p 8000:8000 mypollsapp` to interactively run the django server
* Press `Control+C` on mac to quit the django server

### How to use ###

* Go to [localhost:8000/polls](http://localhost:8000/polls "Go to polls page") on your browser to see the polls questions list
![Polls Questions](readme_images/img2.png) 
&nbsp;

* Click on a particular question to vote for the question
![Vote for Question](readme_images/img3.png) 
&nbsp;

* After successfully voting you would be redirected to results page of that particular question
![Results](readme_images/img4.png) 

&nbsp;
