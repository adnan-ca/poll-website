FROM python:3

# Set environment variables
ENV PYTHONUNBUFFERED 1

# Install dependencies.
RUN pip install django django-crispy-forms

# Set work directory.
RUN mkdir /app
WORKDIR /app

# Copy project code.
COPY . /app/

EXPOSE 8000

CMD python manage.py runserver 0.0.0.0:8000